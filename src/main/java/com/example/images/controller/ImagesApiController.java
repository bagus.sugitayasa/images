/**
 * 
 */
package com.example.images.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@RestController
public class ImagesApiController {

	// http://app/image/thumbnail
	@GetMapping("/api/getImages")
	public Map<String, String> getImages(){
		
		Map<String, String> images = new HashMap<>();
		images.put("image-1", "https://cdn2.techadvisor.co.uk/cmsdata/reviews/3660792/mbp_2017_31_screen.jpg");
		images.put("images2", "http://www.mobilenewsmag.com/wp-content/uploads/2017/05/macbook-pro-2016.jpg");
		return images;
	}
}
